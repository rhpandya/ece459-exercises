// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    if n == 0{
        return 0;
    }
    else if n == 1{
        return 1;
    }
    let mut n1 =  0;
    let mut n2 = 1;
    let mut term= 0;
    let mut counter = 2;
    loop{
        if counter > n{
            break
        }
        term = n1+n2;
        n1 = n2;
        n2 = term;
        counter += 1;
    }
    return term;
}


fn main() {
    println!("{}", fibonacci_number(10));
}
